$(function() {

    "use strict";

    var topoffset = 20; //variable for menu height

    //Activate ScrollSpy
    $('body').scrollspy({
        target: 'header .navbar',
        offset: topoffset
    });
    //It adds a class to the header nav in order to apply styles when in other section when page is loaded
    var hash = $(this).find('li.active a').attr('href');
    if(hash !== '#featured') {
        $('header nav').addClass('inbody');
    } else {
        $('header nav').removeClass('inbody');
    }
    // It does the same when scrollspy event fires
    $('.navbar-fixed-top').on('activate.bs.scrollspy', function() {
        var hash = $(this).find('li.active a').attr('href');
        if(hash !== '#featured') {
        $('header nav').addClass('inbody');
        } else {
        $('header nav').removeClass('inbody');
        }
    });

    //It configures the carousel options
    $('#featured').carousel({
    interval: 10000, pause:"hover"
    });

});



